// Adding the DOM content which help the webpage to be loaded.
 document.addEventListener("DOMContentLoaded", async (event) => { 
// Initialisation of the cards array.
  const cards =[]
// Initialisation of the 'res' constant which is taking the value of the link
  const res = await axios.get('https://jsonplaceholder.typicode.com/posts');
// Initialisation of the 'datas' constant which is taking the value of the res constant
  const datas = res.data;
// Initialisation of the input constant and picking the input element 
  const input = document.querySelector("input")
// Initialisation of the button constant and picking the button01 ID
  const button= document.querySelector('#button01')
// Selection of all the datas.
  datas.forEach(data => {
// Initialisation of the 'div' constant and giving the two parameters 'data.title and data.body' to the 'generatecard constant'
    const div = generateCard(data.title,data.body)
// Display the cards which will be displayed thanks to the div
    cards.push(div)
// picking the container and moving it thanks to the appenchild commands
    document.querySelector('#container').appendChild(div);
  });
// Adding the event 'click' to the button, meaning that when i click there will be something happen.
  button.addEventListener('click', async (e)=>{
// Selecting all the cards displayed on the index
    cards.forEach(card=>{
// Remove all the card displayed
      card.remove()
    })
// Iniatialisation of the res constant and calling the value of the link
    const res = await axios.get(`https://jsonplaceholder.typicode.com/posts?title_like=${input.value}`)
// Initialisation of the datas constant and giving the res value to data
    const datas = res.data
// Selectionning all the datas 
    datas.forEach(data => {
// Initialisation of the 'div' constant, giving to div the value of generateCard with in parameter the data.title and the data.body
      const div = generateCard(data.title,data.body)
// Sending the div value on the document
      cards.push(div)
// Selecting the container ID and moving it into the div
      document.querySelector('#container').appendChild(div); 
    });
  }) 
});
// Creating the function named generateCard which have in parameters title and body
function generateCard(title,body){
// Initialisation of the 'div' constant and create the div element 
  const div = document.createElement('div');
//  Getting the html program about the div
  div.innerHTML = 
// I select each one of the class i need in my html program
  `
  <div class=Title-One>
  <h2>${title}</h2>
  <p>${body}</p>
</div>
        `;
// Adding an event,the click event, mean that when i will click i will have an alert
  div.addEventListener('click', (even) => {
    alert(title);
  });
// Returning the div value instead of void 
  return div;
}